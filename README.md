# TTM00-FW-PIC32-Training-V2

## 1. Description

Training to a firmware and software development on PIC32 board. 
Also, it uses version control and good coding tecniques.

## 2. Hardware description

- PIC32MX470512H: [Datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/PIC32MX330350370430450470_Datasheet_DS60001185H.pdf)
- User guide: [PIC32MX470 Curiosity board](https://ww1.microchip.com/downloads/en/DeviceDoc/70005283B.pdf)

## 3. Serial commands

## 4. Prerequisites

> ### 1. SDK Version

>> - IDE version: MPLAB v5.50
>> - Compiler version: XC32 v1.42
>> - Project configuration:
>>> - Categories: Microchip Embedded
>>> - Projects: Standalone Project
>>> - Family: All families
>>> - Device: PIC32MX470512H
>>> - Hardware tools: Microchip starter kits
>>> - Compiler: XC32 (v1.42)
>>> - Encoding: ISO-8859-1

## 5. Versioning

> ### 1. Current version of the FW
>> - V1.0.20210713

## 6. Authors

> 1. Project staff: Andrés Felipe Romo Guerrero
> 2. Maintainer contact email: aromog@unal.edu.co